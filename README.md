# flatpak

App distribution https://flatpak.org/

## Bug encontered and solved
### No GSettings schemas are installed on the system
$ flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

(flatpak remote-add:29): GLib-GIO-ERROR **: 04:43:28.885: No GSettings schemas are installed on the system
Trace/breakpoint trap (core dumped)

https://gitlab.com/alpinelinux-packages-demo/flatpak/-/jobs/196462874

Solved by requiring "gsettings-desktop-schemas" apk package